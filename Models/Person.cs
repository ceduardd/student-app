﻿using System;

namespace Models
{
    public class Person
    {
        private string name;
        private string cedula;
        private DateTime birthDate;
        private int age;
        private double weight;
        private string gender;

        public string Name { get => name; set => name = value; }
        public string Cedula { get => cedula; set => cedula = value; }
        public DateTime BirthDate { get => birthDate; set => birthDate = value; }
        public int Age { get => age; set => age = value; }
        public double Weight { get => weight; set => weight = value; }
        public string Gender { get => gender; set => gender = value; }

        public Person(string name, string cedula, DateTime birthDate, double weight, string gender)
        {
            this.name = name;
            this.cedula = cedula;
            this.birthDate = birthDate;
            this.weight = weight;
            this.gender = gender;
            age = CalculateAge();
        }

        public int CalculateAge()
        {
            return DateTime.Now.Year - birthDate.Year;
        }

        public override string ToString()
        {
            return $"Nombre: { name }" + Environment.NewLine +
                $"Cédula: { cedula }" + Environment.NewLine +
                $"Fecha de nacimiento: { birthDate }" + Environment.NewLine +
                $"Edad: { age } años" + Environment.NewLine +
                $"Peso: { weight } Kg" + Environment.NewLine +
                $"Sexo: { gender }";
        }
    }
}