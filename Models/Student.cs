﻿using System;

namespace Models
{
    public class Student : Person
    {
        private string career;

        public Student(string name, string cedula, DateTime birthDate, double weight, string gender, string career) : base(name, cedula, birthDate, weight, gender)
        {
            Career = career;
        }

        public string Career { get => career; set => career = value; }

        public override string ToString()
        {
            return base.ToString() + Environment.NewLine +
                $"Carrera: { Career }" + Environment.NewLine;
        }
    }
}