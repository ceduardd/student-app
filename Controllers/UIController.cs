﻿using Models;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Controllers
{
    public class UIController
    {
        StudentController studentController = StudentController.GetInstance();

        public void ShowStudentsInGrid(DataGridView dataGridView)
        {
            int index = 1;
            studentController.Students.ForEach(student => 
                dataGridView.Rows.Add(index++, student.Name, student.Cedula, student.BirthDate.Date, student.Gender, student.Weight, student.Career)
            );
        }

        public void WriteStudentsCount(Label label)
        {
            label.Text = studentController.StudentsCount() + "";
        }

        public void RemoveStudentFromGrid(int index, DataGridView dataGridView)
        {
            studentController.DeleteStudent(index);
            dataGridView.Rows.RemoveAt(index);
        }

        public void ShowStudentFilterResults(string gender, string cedula, DataGridView dataGridView)
        {
            dataGridView.Rows.Clear();

            List<Student> studentFilterResults = studentController.FilterStudents(gender, cedula);
            
            int index = 1;
            studentFilterResults.ForEach(student => 
                dataGridView.Rows.Add(index++, student.Name, student.Cedula, student.BirthDate.Date, student.Gender, student.Weight, student.Career)
            );
        }
    }
}
