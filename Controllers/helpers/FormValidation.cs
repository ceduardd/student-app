﻿using System;

namespace Controllers
{
    public class FormValidation
    {
        public bool IsValidWeight(string weight)
        {
            bool isValidWeight = false;
            try
            {
                Double.Parse(weight);
                isValidWeight = true;
            }
            catch (FormatException e)
            {
                Console.WriteLine(e);
            }

            return isValidWeight;
        }

        public bool IsValidCedula(string cedula) => cedula.Length < 10 ? false : true;

        public bool AnyFieldIsEmpty(string name, string cedula, DateTime birthDate, string gender, string weight, string career)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(cedula) || string.IsNullOrEmpty(gender) || string.IsNullOrEmpty(weight) || string.IsNullOrEmpty(career) || birthDate == null)
            {
                return true;
            }

            return false;
        }

        public bool IsCedulaEmpty(string cedula) => string.IsNullOrEmpty(cedula);
    }
}