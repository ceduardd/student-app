﻿using System;

namespace Controllers
{
    public class FormatData
    {
        public double WeightStringToNumber(string weight) => Double.Parse(weight);
    }
}
