﻿using System;
using System.Collections.Generic;
using Models;

namespace Controllers
{
    public class StudentController
    {
        private List<Student> students;
        private static StudentController studentController;

        private StudentController()
        {
            students = new List<Student>();

            students.Add(new Student("Wilmer Chávez", "0943984765", new DateTime(1999, 12, 14), 80.0, "Masculino", "Carrera de Software"));
            students.Add(new Student("Jhon Doe", "0915448310", new DateTime(2008, 8, 9), 60.0, "Masculino", "Carrera de Software"));
            students.Add(new Student("Jane Doe", "0990398910", new DateTime(2000, 10, 29), 78.0, "Femenino", "Carrera de Ing. Sistemas"));
        }

        // Singleton pattern
        public static StudentController GetInstance()
        {
            if (studentController == null)
            {
                studentController = new StudentController();
            }

            return studentController;
        }

        internal List<Student> Students { get => students; }

        internal void DeleteStudent(int index) => students.RemoveAt(index);

        // Filter students by gender and cedula
        internal List<Student> FilterStudents(string gender, string cedula)
        {
            if (gender == "Masculino")
            {
                return students.FindAll(student => student.Gender == "Masculino" && student.Cedula.StartsWith(cedula));
            } else if (gender == "Femenino")
            {
                return students.FindAll(student => student.Gender == "Femenino" && student.Cedula.StartsWith(cedula));
            } else
            {
                return students.FindAll(student => student.Cedula.StartsWith(cedula));
            }
        }

        // public List<Student> GetStudents() => students;

        private Student GetStudentByCedula(string cedula) => students.Find(student => student.Cedula == cedula);
        public string GetStudentInfoByCedula(string cedula) => GetStudentByCedula(cedula).ToString();

        public bool AlreadyExist(string cedula) => students.Exists(student => student.Cedula == cedula);

        public string Save(string name, string cedula, DateTime birthDate, string gender, double weight, string career)
        {
            Student student = new Student(name, cedula, birthDate, weight, gender, career);
            students.Add(student);

            return student.ToString();
        }

        public int StudentsCount() => students.Count;

        public int GetIndexOf(string cedula)
        {
            Student studentFound = GetStudentByCedula(cedula);

            return students.IndexOf(studentFound);
        }

        public void Update(int index, string name, string cedula, DateTime birthDate, string gender, double weight, string career)
        {
            Student student = new Student(name, cedula, birthDate, weight, gender, career);
            students.RemoveAt(index);
            students.Insert(index, student);
            
        }
    }
}
