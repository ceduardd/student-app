﻿using System;
using System.Windows.Forms;
using Controllers;

namespace Views
{
    public partial class DeleteStudentScreen : Form
    {
        StudentController studentController = StudentController.GetInstance();
        UIController uiController = new UIController();

        public DeleteStudentScreen()
        {
            InitializeComponent();
            uiController.ShowStudentsInGrid(dataGridStudents);
        }

        private void DeleteStudent(object sender, EventArgs e)
        {
            if (studentController.StudentsCount() > 0)
            {
                int index = dataGridStudents.CurrentRow.Index;

                uiController.RemoveStudentFromGrid(index, dataGridStudents);
            } else
            {
                MessageBox.Show("No hay registros de estudiantes", "Alerta");
            }
        }
    }
}
