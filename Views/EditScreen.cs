﻿using System;
using System.Windows.Forms;
using Controllers;

namespace Views
{
    public partial class EditScreen : Form
    {
        UIController uiController = new UIController();
        public EditScreen()
        {
            InitializeComponent();

            uiController.ShowStudentsInGrid(dataGridStudents);
        }

        private void ShowEditStudentForm(object sender, EventArgs e)
        {
            DataGridViewCellCollection cells = dataGridStudents.CurrentRow.Cells;

            /*foreach (DataGridViewCell cell in cells)
            {
                Console.WriteLine(cell.Value.ToString());
            }*/

            string name = cells[1].Value.ToString();
            string cedula = cells[2].Value.ToString();
            DateTime birthDate = DateTime.Parse(cells[3].Value.ToString());
            string weight = cells[4].Value.ToString();
            string gender = cells[5].Value.ToString();
            string career = cells[6].Value.ToString();

            EditStudentForm editStudentForm = new EditStudentForm(name, cedula, birthDate, gender, weight, career, dataGridStudents);
            editStudentForm.ShowDialog();
        }
    }
}
