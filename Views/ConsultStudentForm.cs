﻿using System;
using System.Windows.Forms;
using Controllers;

namespace Views
{
    public partial class ConsultStudentForm : Form
    {
        StudentController studentController = StudentController.GetInstance();

        FormValidation formValidation = new FormValidation();

        public ConsultStudentForm()
        {
            InitializeComponent();
        }

        // Show seacrh result
        private void ShowConsultResults(object sender, EventArgs e)
        {
            string cedula = cedulaInput.Text;

            if (!formValidation.IsCedulaEmpty(cedula))
            {
                string studentFound = studentController.GetStudentInfoByCedula(cedula);
                if (studentFound != null)
                {
                    dataContainer.Text += studentFound;
                } else
                {
                    MessageBox.Show($"No se encontró estudiante con cedula { cedula }", "Alerta");
                }
            }
            else
            {
                MessageBox.Show("El campo cedula no puede estar vacío", "Alerta");
            }
        }

        // Input constraints
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Allowed only numbers
            if (!char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
                return;
            }
        }
    }
}
