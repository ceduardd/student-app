﻿using System;
using System.Windows.Forms;
using Controllers;

namespace Views
{
    public partial class HomeScreen : Form
    {
        StudentController studentController = StudentController.GetInstance();

        public HomeScreen()
        {
            InitializeComponent();
        }

        private void ShowAddStudentForm(object sender, EventArgs e)
        {
            AddStudentForm addStudentForm = new AddStudentForm();
            addStudentForm.ShowDialog();
        }

        private void ShowStudentListScreen(object sender, EventArgs e)
        {
            if (studentController.StudentsCount() > 0)
            {
                StudentListScreen studentListScreen = new StudentListScreen();
                studentListScreen.ShowDialog();
            }
            else
            {
                MessageBox.Show("No hay estudiantes registrados", "Alerta");
            }
        }

        /*private void ShowConsultStudentForm(object sender, EventArgs e)
        {
            ConsultStudentForm consultStudentForm = new ConsultStudentForm();
            consultStudentForm.ShowDialog();
        }*/

        private void ShowDeleteStudentScreen(object sender, EventArgs e)
        {
            if (studentController.StudentsCount() > 0)
            {
                DeleteStudentScreen deleteStudentScreen = new DeleteStudentScreen();
                deleteStudentScreen.ShowDialog();
            } else
            {
                MessageBox.Show("No hay estudiantes registrados", "Alerta");
            }
        }

        private void ShowStudentFilterScreen(object sender, EventArgs e)
        {
            if (studentController.StudentsCount() > 0)
            {
                StudentFilterScreen searchStudent = new StudentFilterScreen();
                searchStudent.ShowDialog();
            }
            else
            {
                MessageBox.Show("No hay estudiantes registrados", "Alerta");
            }
        }

        private void ShowEditScreen(object sender, EventArgs e)
        {
            if (studentController.StudentsCount() > 0)
            {
                EditScreen editScreen = new EditScreen();
                editScreen.ShowDialog();
            }
            else
            {
                MessageBox.Show("No hay estudiantes registrados", "Alerta");
            }
        }
    }
}
