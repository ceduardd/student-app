﻿using System;
using System.Windows.Forms;
using Controllers;

namespace Views
{
    public partial class AddStudentForm : Form
    {
        StudentController studentController = StudentController.GetInstance();
        
        // This could be improved
        FormValidation formValidation = new FormValidation();
        FormatData formatData = new FormatData();

        public AddStudentForm()
        {
            InitializeComponent();
        }

        // Send correct data to create and add new student
        private void AddStudentToList(object sender, EventArgs e)
        {
            string name = nameInput.Text.Trim();
            string cedula = cedulaInput.Text.Trim();
            DateTime birthDate = birthDateInput.Value.Date;
            string weight = weightInput.Text.Trim();
            string gender = genderInput.Text;
            string career = careerInput.Text;

            if (!studentController.AlreadyExist(cedula))
            {
                if (formValidation.AnyFieldIsEmpty(name, cedula, birthDate, gender, weight, career))
                {
                    MessageBox.Show("Todos los campos son obligatorios", "Alerta");
                }
                else if (!formValidation.IsValidWeight(weight))
                {
                    MessageBox.Show("El peso ingresado no tiene el formato correcto");
                }
                else if (!formValidation.IsValidCedula(cedula))
                {
                    MessageBox.Show("La cedula ingresada no tiene el formato correcto");
                }
                else
                {
                    double parseWeight = formatData.WeightStringToNumber(weight);

                    // Save info into student list
                    string studentData = studentController.Save(name, cedula, birthDate, gender, parseWeight, career);

                    // Show recently info added on TexBox
                    dataContainer.Text += studentData;
                }
            } else
            {
                MessageBox.Show($"La cedula { cedula } ya existe!");
            }
        }

        // Input constraints
        private void nameInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            // A-Z, a-z, spaces, back
            if (!char.IsLetter(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back) && e.KeyChar != Convert.ToChar(Keys.Space))
            {
                e.Handled = true;
                return;
            }
        }

        private void cedulaInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Only numbers
            if (!char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
                return;
            }
        }

        private void weightInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Only numbers with dot
            if (!char.IsNumber(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back) && e.KeyChar != '.')
            {
                e.Handled = true;
                return;
            }
        }
    }
}
