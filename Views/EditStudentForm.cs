﻿using System;
using System.Windows.Forms;
using Controllers;

namespace Views
{
    public partial class EditStudentForm : Form
    {
        // Controllers
        StudentController studentController = StudentController.GetInstance();
        UIController uiController = new UIController();

        // Validators
        FormValidation formValidation = new FormValidation();
        FormatData formatData = new FormatData();

        // References
        DataGridView dataGridViewRef = null;
        private string initialCedula = "";

        public EditStudentForm(string name, string cedula, DateTime birthDate, string weight, string gender, string career, DataGridView dataGridView)
        {
            InitializeComponent();

            nameInput.Text = name;
            cedulaInput.Text = cedula;
            birthDateInput.Value = birthDate;
            weightInput.Text = weight;
            genderInput.Text = gender;
            careerInput.Text = career;

            initialCedula = cedula;

            dataGridViewRef = dataGridView;
        }

        private void UpdateStudentData(object sender, EventArgs e)
        {
            int index = studentController.GetIndexOf(initialCedula);

            string name = nameInput.Text.Trim();
            string cedula = cedulaInput.Text.Trim();
            DateTime birthDate = birthDateInput.Value.Date;
            string weight = weightInput.Text.Trim();
            string gender = genderInput.Text;
            string career = careerInput.Text;

            if (formValidation.AnyFieldIsEmpty(name, cedula, birthDate, gender, weight, career))
            {
                MessageBox.Show("Todos los campos son obligatorios", "Alerta");
                return;
            }
            else if (!formValidation.IsValidWeight(weight))
            {
                MessageBox.Show("El peso ingresado no tiene el formato correcto");
                return;
            }
            else if (!formValidation.IsValidCedula(cedula))
            {
                MessageBox.Show("La cedula ingresada no tiene el formato correcto");
                return;
            }
            else if (cedula != initialCedula)
            {
                if (studentController.AlreadyExist(cedula))
                {
                    MessageBox.Show($"La cedula { cedula } ya existe!");
                    return;
                }
            }

            double parseWeight = formatData.WeightStringToNumber(weight);

            // Save info into student list
            studentController.Update(index, name, cedula, birthDate, gender, parseWeight, career);

            MessageBox.Show("Registro actualizado", "Alerta");

            OnUpdated();
        }

        private void OnUpdated()
        {
            this.Close();
            dataGridViewRef.Rows.Clear();
            uiController.ShowStudentsInGrid(dataGridViewRef);
        }
    }
}
