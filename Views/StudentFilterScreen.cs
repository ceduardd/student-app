﻿using System;
using System.Windows.Forms;
using Controllers;

namespace Views
{
    public partial class StudentFilterScreen : Form
    {
        StudentController studentController = StudentController.GetInstance();
        UIController uiController = new UIController();

        public StudentFilterScreen()
        {
            InitializeComponent();
        }

        private void ShowStudentFilterResults(object sender, EventArgs e)
        {
            string gender = genderInput.Text;
            string cedula = cedulaInput.Text;

            if (gender != "" && cedula != "")
            {
                uiController.ShowStudentFilterResults(gender, cedula, dataGridStudents);
            } else
            {
                MessageBox.Show("Los campos son obligatorios", "Alerta");
            }
        }
    }
}
