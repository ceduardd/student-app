﻿using System.Windows.Forms;
using Controllers;

namespace Views
{
    public partial class StudentListScreen : Form
    {
        UIController uiController = new UIController();

        public StudentListScreen()
        {
            InitializeComponent();

            uiController.ShowStudentsInGrid(dataGridStudents);
            uiController.WriteStudentsCount(lblTotal);
        }
    }
}
