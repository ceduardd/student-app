﻿
namespace Views
{
    partial class DeleteStudentScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridStudents = new System.Windows.Forms.DataGridView();
            this.numCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cedulaCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.birthDateCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.genderCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.weightCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.careerCol = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridStudents)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridStudents
            // 
            this.dataGridStudents.AllowUserToAddRows = false;
            this.dataGridStudents.AllowUserToDeleteRows = false;
            this.dataGridStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridStudents.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.numCol,
            this.nameCol,
            this.cedulaCol,
            this.birthDateCol,
            this.genderCol,
            this.weightCol,
            this.careerCol});
            this.dataGridStudents.Location = new System.Drawing.Point(31, 66);
            this.dataGridStudents.Name = "dataGridStudents";
            this.dataGridStudents.ReadOnly = true;
            this.dataGridStudents.Size = new System.Drawing.Size(738, 304);
            this.dataGridStudents.TabIndex = 5;
            // 
            // numCol
            // 
            this.numCol.HeaderText = "Nro.";
            this.numCol.Name = "numCol";
            this.numCol.ReadOnly = true;
            // 
            // nameCol
            // 
            this.nameCol.HeaderText = "Nombre";
            this.nameCol.Name = "nameCol";
            this.nameCol.ReadOnly = true;
            // 
            // cedulaCol
            // 
            this.cedulaCol.HeaderText = "Cédula";
            this.cedulaCol.Name = "cedulaCol";
            this.cedulaCol.ReadOnly = true;
            // 
            // birthDateCol
            // 
            this.birthDateCol.HeaderText = "Fecha de Nacimiento";
            this.birthDateCol.Name = "birthDateCol";
            this.birthDateCol.ReadOnly = true;
            // 
            // genderCol
            // 
            this.genderCol.HeaderText = "Sexo";
            this.genderCol.Name = "genderCol";
            this.genderCol.ReadOnly = true;
            // 
            // weightCol
            // 
            this.weightCol.HeaderText = "Peso (Kg)";
            this.weightCol.Name = "weightCol";
            this.weightCol.ReadOnly = true;
            // 
            // careerCol
            // 
            this.careerCol.HeaderText = "Carrera";
            this.careerCol.Name = "careerCol";
            this.careerCol.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(68)))), ((int)(((byte)(72)))));
            this.label1.Location = new System.Drawing.Point(27, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(196, 22);
            this.label1.TabIndex = 6;
            this.label1.Text = "Estudiantes a Eliminar";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(357, 388);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Eliminar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.DeleteStudent);
            // 
            // DeleteStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridStudents);
            this.Name = "DeleteStudent";
            this.Text = "DeleteStudent";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridStudents)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridStudents;
        private System.Windows.Forms.DataGridViewTextBoxColumn numCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn cedulaCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn birthDateCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn genderCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn weightCol;
        private System.Windows.Forms.DataGridViewTextBoxColumn careerCol;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}